# delivery-kata

## Introduction
Hello, this kata is made by "Jesser Abidelli"

## Technologies
Spring Boot 3.1.5

java 21

MongoDB

Kafka

Docker

## Lancer le projet
Pour lancer le projet, on peut utiliser le fichier docker compose déjà présent dans le repo en utilisant la commande suivante :

```
docker-compose up
```

Nous allons disposer d'une base de données MongoDB avec l'outil mongo express pour visualiser le contenu de la base.
De plus, nous aurons Kafka avec KafkaHQ pour observer les topics Kafka.

Ensuite, on peut cloner le projet et le lancer (attention à la version de Java 21).

## Structure du projet
Le projet suit une architecture hexagonale, avec les packages suivants :
- **domain** : defini les entités et notre métier
- **application** : expose les endpoints REST
- **infrastructure** : contient les classes qui interagissent avec les technologies 
utilisées pour la persistance et la messagerie (MongoDB, Kafka).

## Explication de l'implémentation

1. J'ai opté pour une architecture hexagonale, qui nous permettra de nous concentrer sur le domaine et le métier 
tout en les découplant des choix techniques.

2. Utilisation de mapstruct pour l'échange des données entre les couches de notre architecture et pour 
la présentation à l'extérieur à l'aide des DTOs.

3. Développer une solution non bloquante avec Spring Reactive.
4. Sécuriser l'application avec Spring Security.
5. Utilisation de Kafka pour l'envoi des "deliveries" qui seront consommées par un autre service.
