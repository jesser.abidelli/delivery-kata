FROM openjdk:21-jdk-slim

RUN apt -y update && apt -y install unzip

ARG artifact_id="delivery"
ARG application_name="delivery"
ARG application_root_dir="/kata"

ENV ARTIFACT_ID=${artifact_id}
ENV APPLICATION_DIR=${application_root_dir}/${application_name}

RUN mkdir -p ${APPLICATION_DIR}/bin ${APPLICATION_DIR}/etc ${APPLICATION_DIR}/certs

COPY ./target/${ARTIFACT_ID}*.jar ${APPLICATION_DIR}/bin/${ARTIFACT_ID}.jar

EXPOSE 8070

CMD java -jar ${APPLICATION_DIR}/bin/${ARTIFACT_ID}.jar