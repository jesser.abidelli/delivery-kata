package com.kata.delivery.infrastructure.mongoadapter;

import com.kata.delivery.domain.model.Client;
import com.kata.delivery.infrastructure.mongoadapter.entity.ClientEntity;
import com.kata.delivery.infrastructure.mongoadapter.mapper.ClientEntityMapper;
import com.kata.delivery.infrastructure.mongoadapter.repository.ClientEntityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.Mockito.when;

class ClientMongoAdapterTest {
    @Mock
    private ClientEntityRepository clientEntityRepository;

    @Mock
    private ClientEntityMapper clientEntityMapper;

    @InjectMocks
    private ClientMongoAdapter clientMongoAdapter;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testSave() {
        Client client = new Client("username", "email", "password","address","phone",null);
        ClientEntity clientEntity = new ClientEntity("username", "email", "password","address","phone",null);
        when(clientEntityMapper.toEntity(client)).thenReturn(clientEntity);
        when(clientEntityRepository.save(clientEntity)).thenReturn(Mono.just(clientEntity));
        when(clientEntityMapper.toDomain(clientEntity)).thenReturn(client);

        StepVerifier.create(clientMongoAdapter.save(client))
                .expectNext(client)
                .verifyComplete();
    }

    @Test
    void testUpdate(){
        Client client = new Client("username", "email", "password","address","phone",null);
        ClientEntity clientEntity = new ClientEntity("username", "email", "password","address","phone",null);
        when(clientEntityMapper.toEntity(client)).thenReturn(clientEntity);
        when(clientEntityRepository.save(clientEntity)).thenReturn(Mono.just(clientEntity));
        when(clientEntityMapper.toDomain(clientEntity)).thenReturn(client);

        StepVerifier.create(clientMongoAdapter.update(client))
                .expectNext(client)
                .verifyComplete();
    }

    @Test
    void testGetWhenClientExists(){
        Client client = new Client("username", "email", "password","address","phone",null);
        ClientEntity clientEntity = new ClientEntity("username", "email", "password","address","phone",null);
        when(clientEntityMapper.toEntity(client)).thenReturn(clientEntity);
        when(clientEntityRepository.findById("username")).thenReturn(Mono.just(clientEntity));
        when(clientEntityMapper.toDomain(clientEntity)).thenReturn(client);

        StepVerifier.create(clientMongoAdapter.get("username"))
                .expectNext(client)
                .verifyComplete();
    }

    @Test
    void testGetWhenClientDoesNotExist(){
        when(clientEntityRepository.findById("username")).thenReturn(Mono.empty());

        StepVerifier.create(clientMongoAdapter.get("username"))
                .expectNextCount(0)
                .verifyComplete();
    }

    @Test
    void testfindAll(){
        Client client = new Client("username", "email", "password","address","phone",null);
        ClientEntity clientEntity = new ClientEntity("username", "email", "password","address","phone",null);
        when(clientEntityMapper.toEntity(client)).thenReturn(clientEntity);
        when(clientEntityRepository.findAll()).thenReturn(Flux.just(clientEntity));
        when(clientEntityMapper.toDomain(clientEntity)).thenReturn(client);

        StepVerifier.create(clientMongoAdapter.findAll())
                .expectNext(client)
                .verifyComplete();
    }
}
