package com.kata.delivery.infrastructure.mongoadapter;

import com.kata.delivery.domain.model.Order;
import com.kata.delivery.infrastructure.mongoadapter.entity.OrderEntity;
import com.kata.delivery.infrastructure.mongoadapter.mapper.OrderEntityMapper;
import com.kata.delivery.infrastructure.mongoadapter.repository.OrderEntityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;


import static org.mockito.Mockito.when;

class OrderMongoAdapterTest {
    @Mock
    private OrderEntityRepository orderEntityRepository;

    @Mock
    private OrderEntityMapper orderEntityMapper;

    @InjectMocks
    private OrderMongoAdapter orderMongoAdapter;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testSave() {
        Order order = new Order("1", null);
        OrderEntity orderEntity = new OrderEntity("1","1", null);
        when(orderEntityMapper.toEntity(order)).thenReturn(orderEntity);
        when(orderEntityRepository.save(orderEntity)).thenReturn(Mono.just(orderEntity));
        when(orderEntityMapper.toDomain(orderEntity)).thenReturn(order);

        StepVerifier.create(orderMongoAdapter.save(order))
                .expectNext(order)
                .verifyComplete();
    }

    @Test
    void testUpdate(){
        Order order = new Order("1", null);
        OrderEntity orderEntity = new OrderEntity("1","1", null);
        when(orderEntityMapper.toEntity(order)).thenReturn(orderEntity);
        when(orderEntityRepository.save(orderEntity)).thenReturn(Mono.just(orderEntity));
        when(orderEntityMapper.toDomain(orderEntity)).thenReturn(order);

        StepVerifier.create(orderMongoAdapter.update(order))
                .expectNext(order)
                .verifyComplete();
    }

    @Test
    void testGetWhenOrderExists(){
        Order order = new Order("1", null);
        OrderEntity orderEntity = new OrderEntity("1","1", null);
        when(orderEntityRepository.findById("1")).thenReturn(Mono.just(orderEntity));
        when(orderEntityMapper.toDomain(orderEntity)).thenReturn(order);

        StepVerifier.create(orderMongoAdapter.get("1"))
                .expectNext(order)
                .verifyComplete();
    }

    @Test
    void testGetWhenOrderDoesNotExist(){
        when(orderEntityRepository.findById("1")).thenReturn(Mono.empty());

        StepVerifier.create(orderMongoAdapter.get("1"))
                .expectNextCount(0)
                .verifyComplete();
    }

    @Test
    void testGetAll(){
        Order order = new Order("1", null);
        OrderEntity orderEntity = new OrderEntity("1","1", null);
        when(orderEntityRepository.findAll()).thenReturn(Flux.just(orderEntity));
        when(orderEntityMapper.toDomain(orderEntity)).thenReturn(order);

        StepVerifier.create(orderMongoAdapter.getAll())
                .expectNext(order)
                .verifyComplete();
    }
}
