package com.kata.delivery.infrastructure.mongoadapter;

import com.kata.delivery.domain.model.Delivery;
import com.kata.delivery.domain.model.DeliveryMode;
import com.kata.delivery.infrastructure.mongoadapter.entity.DeliveryEntity;
import com.kata.delivery.infrastructure.mongoadapter.mapper.DeliveryEntityMapper;
import com.kata.delivery.infrastructure.mongoadapter.repository.DeliveryEntityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.Mockito.*;

class DeliveryMongoAdapterTest {
    @Mock
    private DeliveryEntityRepository deliveryEntityRepository;

    @Mock
    private DeliveryEntityMapper deliveryEntityMapper;

    @InjectMocks
    private DeliveryMongoAdapter deliveryMongoAdapter;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testSave() {
        Delivery delivery = new Delivery("1","delivery","description", DeliveryMode.DELIVERY ,"adress",null,"timeSlotId", null);
        DeliveryEntity deliveryEntity = new DeliveryEntity("1","delivery","description", DeliveryMode.DELIVERY ,"adress",null,"timeSlotId", null);
        when(deliveryEntityMapper.toEntity(delivery)).thenReturn(deliveryEntity);
        when(deliveryEntityMapper.toDomain(deliveryEntity)).thenReturn(delivery);
        when(deliveryEntityRepository.save(deliveryEntity)).thenReturn(Mono.just(deliveryEntity));

        Mono<Delivery> result = deliveryMongoAdapter.save(delivery);

        StepVerifier.create(result)
                .expectNext(delivery)
                .verifyComplete();
    }

    @Test
    void testUpdate(){
        Delivery delivery = new Delivery("1","delivery","description", DeliveryMode.DELIVERY ,"adress",null,"timeSlotId", null);
        DeliveryEntity deliveryEntity = new DeliveryEntity("1","delivery","description", DeliveryMode.DELIVERY ,"adress",null,"timeSlotId", null);
        when(deliveryEntityMapper.toEntity(delivery)).thenReturn(deliveryEntity);
        when(deliveryEntityMapper.toDomain(deliveryEntity)).thenReturn(delivery);
        when(deliveryEntityRepository.save(deliveryEntity)).thenReturn(Mono.just(deliveryEntity));

        StepVerifier.create(deliveryMongoAdapter.update(delivery))
                .expectNext(delivery)
                .verifyComplete();
    }

    @Test
    void testGet(){
        String id = "1";
        Delivery delivery = new Delivery("1","delivery","description", DeliveryMode.DELIVERY ,"adress",null,"timeSlotId", null);
        DeliveryEntity deliveryEntity = new DeliveryEntity("1","delivery","description", DeliveryMode.DELIVERY ,"adress",null,"timeSlotId", null);
        when(deliveryEntityMapper.toDomain(deliveryEntity)).thenReturn(delivery);
        when(deliveryEntityRepository.findById(id)).thenReturn(Mono.just(deliveryEntity));

        StepVerifier.create(deliveryMongoAdapter.get(id))
                .expectNext(delivery)
                .verifyComplete();
    }

    @Test
    void testGetWhenDeliveryEntityNotFound(){
        String id = "1";
        when(deliveryEntityRepository.findById(id)).thenReturn(Mono.empty());

        StepVerifier.create(deliveryMongoAdapter.get(id))
                .expectNextCount(0)
                .verifyComplete();
    }

    @Test
    void testGetAll(){
        Delivery delivery = new Delivery("1","delivery","description", DeliveryMode.DELIVERY ,"adress",null,"timeSlotId", null);
        DeliveryEntity deliveryEntity = new DeliveryEntity("1","delivery","description", DeliveryMode.DELIVERY ,"adress",null,"timeSlotId", null);
        when(deliveryEntityMapper.toDomain(deliveryEntity)).thenReturn(delivery);
        when(deliveryEntityRepository.findAll()).thenReturn(Flux.just(deliveryEntity));

        StepVerifier.create(deliveryMongoAdapter.getAll())
                .expectNext(delivery)
                .verifyComplete();
    }
}