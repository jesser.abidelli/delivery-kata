package com.kata.delivery.infrastructure.mongoadapter;

import com.kata.delivery.domain.model.TimeSlot;
import com.kata.delivery.infrastructure.mongoadapter.entity.TimeSlotEntity;
import com.kata.delivery.infrastructure.mongoadapter.mapper.TimeSlotEntityMapper;
import com.kata.delivery.infrastructure.mongoadapter.repository.TimeSlotEntityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDateTime;

import static org.mockito.Mockito.when;

class TimeSlotMongoAdapterTest {
    @Mock
    private TimeSlotEntityRepository timeSlotEntityRepository;

    @Mock
    private TimeSlotEntityMapper timeSlotEntityMapper;

    @InjectMocks
    private TimeSlotMongoAdapter timeSlotMongoAdapter;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testSave() {
        TimeSlot timeSlot = new TimeSlot(LocalDateTime.parse("2021-01-01T10:00:00"));
        TimeSlotEntity timeSlotEntity = new TimeSlotEntity("1", LocalDateTime.parse("2021-01-01T10:00:00"), null, 1,null);
        when(timeSlotEntityMapper.toEntity(timeSlot)).thenReturn(timeSlotEntity);
        when(timeSlotEntityRepository.save(timeSlotEntity)).thenReturn(Mono.just(timeSlotEntity));
        when(timeSlotEntityMapper.toDomain(timeSlotEntity)).thenReturn(timeSlot);

        StepVerifier.create(timeSlotMongoAdapter.save(timeSlot))
                .expectNext(timeSlot)
                .verifyComplete();
    }

    @Test
    void testUpdate(){
        TimeSlot timeSlot = new TimeSlot(LocalDateTime.parse("2021-01-01T10:00:00"));
        TimeSlotEntity timeSlotEntity = new TimeSlotEntity("1", LocalDateTime.parse("2021-01-01T10:00:00"), null, 1,null);
        when(timeSlotEntityMapper.toEntity(timeSlot)).thenReturn(timeSlotEntity);
        when(timeSlotEntityRepository.save(timeSlotEntity)).thenReturn(Mono.just(timeSlotEntity));
        when(timeSlotEntityMapper.toDomain(timeSlotEntity)).thenReturn(timeSlot);

        StepVerifier.create(timeSlotMongoAdapter.update(timeSlot))
                .expectNext(timeSlot)
                .verifyComplete();
    }

    @Test
    void testGetByIdWhenTimeSlotExists(){
        TimeSlot timeSlot = new TimeSlot(LocalDateTime.parse("2021-01-01T10:00:00"));
        TimeSlotEntity timeSlotEntity = new TimeSlotEntity("1", LocalDateTime.parse("2021-01-01T10:00:00"), null, 1,null);
        when(timeSlotEntityMapper.toEntity(timeSlot)).thenReturn(timeSlotEntity);
        when(timeSlotEntityRepository.findById("1")).thenReturn(Mono.just(timeSlotEntity));
        when(timeSlotEntityMapper.toDomain(timeSlotEntity)).thenReturn(timeSlot);

        StepVerifier.create(timeSlotMongoAdapter.get("1"))
                .expectNext(timeSlot)
                .verifyComplete();
    }

    @Test
    void testGetByIdWhenTimeSlotDoesNotExist(){
        when(timeSlotEntityRepository.findById("1")).thenReturn(Mono.empty());

        StepVerifier.create(timeSlotMongoAdapter.get("1"))
                .expectNextCount(0)
                .verifyComplete();
    }

    @Test
    void testGetAll(){
        TimeSlot timeSlot = new TimeSlot(LocalDateTime.parse("2021-01-01T10:00:00"));
        TimeSlotEntity timeSlotEntity = new TimeSlotEntity("1", LocalDateTime.parse("2021-01-01T10:00:00"), null, 1,null);
        when(timeSlotEntityRepository.findAll()).thenReturn(Flux.just(timeSlotEntity));
        when(timeSlotEntityMapper.toDomain(timeSlotEntity)).thenReturn(timeSlot);

        StepVerifier.create(timeSlotMongoAdapter.getAll())
                .expectNext(timeSlot)
                .verifyComplete();
    }
}
