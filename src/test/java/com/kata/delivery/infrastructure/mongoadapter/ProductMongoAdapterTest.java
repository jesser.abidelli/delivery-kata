package com.kata.delivery.infrastructure.mongoadapter;

import com.kata.delivery.domain.model.Product;
import com.kata.delivery.infrastructure.mongoadapter.entity.ProductEntity;
import com.kata.delivery.infrastructure.mongoadapter.mapper.ProductEntityMapper;
import com.kata.delivery.infrastructure.mongoadapter.repository.ProductEntityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.Mockito.when;

class ProductMongoAdapterTest {
    @Mock
    private ProductEntityRepository productEntityRepository;

    @Mock
    private ProductEntityMapper productEntityMapper;

    @InjectMocks
    private ProductMongoAdapter productMongoAdapter;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testSave() {
        Product product = new Product("1", "name",4.0, "description");
        ProductEntity productEntity = new ProductEntity("1", "name", 4.0, "description");
        when(productEntityMapper.toEntity(product)).thenReturn(productEntity);
        when(productEntityRepository.save(productEntity)).thenReturn(Mono.just(productEntity));
        when(productEntityMapper.toDomain(productEntity)).thenReturn(product);

        StepVerifier.create(productMongoAdapter.save(product))
                .expectNext(product)
                .verifyComplete();
    }

    @Test
    void testUpdate(){
        Product product = new Product("1", "name",4.0, "description");
        ProductEntity productEntity = new ProductEntity("1", "name", 4.0, "description");
        when(productEntityMapper.toEntity(product)).thenReturn(productEntity);
        when(productEntityRepository.save(productEntity)).thenReturn(Mono.just(productEntity));
        when(productEntityMapper.toDomain(productEntity)).thenReturn(product);

        StepVerifier.create(productMongoAdapter.update(product))
                .expectNext(product)
                .verifyComplete();
    }

    @Test
    void testGet(){
        Product product = new Product("1", "name",4.0, "description");
        ProductEntity productEntity = new ProductEntity("1", "name", 4.0, "description");
        when(productEntityRepository.findById("1")).thenReturn(Mono.just(productEntity));
        when(productEntityMapper.toDomain(productEntity)).thenReturn(product);

        StepVerifier.create(productMongoAdapter.get("1"))
                .expectNext(product)
                .verifyComplete();
    }

    @Test
    void testGetAll(){
        Product product = new Product("1", "name",4.0, "description");
        ProductEntity productEntity = new ProductEntity("1", "name", 4.0, "description");
        when(productEntityRepository.findAll()).thenReturn(Flux.just(productEntity));
        when(productEntityMapper.toDomain(productEntity)).thenReturn(product);

        StepVerifier.create(productMongoAdapter.getAll())
                .expectNext(product)
                .verifyComplete();
    }
}
