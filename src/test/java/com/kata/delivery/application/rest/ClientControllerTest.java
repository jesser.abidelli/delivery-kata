package com.kata.delivery.application.rest;

import com.kata.delivery.application.dto.ClientDto;
import com.kata.delivery.application.dto.ClientSignUp;
import com.kata.delivery.application.mapper.ClientDtoMapper;
import com.kata.delivery.application.mapper.ClientSignUpMapper;
import com.kata.delivery.domain.model.Client;
import com.kata.delivery.domain.model.Role;
import com.kata.delivery.domain.port.api.ClientService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebFluxTest(ClientController.class)
class ClientControllerTest {
    @MockBean
    private ClientService clientService;

    @MockBean
    private ClientDtoMapper clientDtoMapper;

    @MockBean
    private ClientSignUpMapper clientSignUpMapper;

    @InjectMocks
    private ClientController clientController;

    @Autowired
    private WebTestClient webTestClient;

    Client client = new Client("username", "email", "password","address","phone", Role.USER);
    ClientDto clientDto = new ClientDto("username", "email", "address", "phone");
    ClientSignUp clientSignUp = new ClientSignUp("username", "email", "password","address","phone");
    @Test
    @WithMockUser(username = "user", roles = "ADMIN")
    void testCreateClient() {
        when(clientService.createClient(client)).thenReturn(Mono.just(client));
        when(clientDtoMapper.toDto(client)).thenReturn(clientDto);
        when(clientSignUpMapper.toDomain(clientSignUp)).thenReturn(client);

        webTestClient
                .mutateWith(SecurityMockServerConfigurers.csrf())
                .post()
                .uri("/api/client")
                .bodyValue(clientSignUp)
                .exchange()
                .expectStatus().isOk()
                .expectBody(ClientDto.class)
                .isEqualTo(clientDto);
    }

    @Test
    @WithMockUser(username = "user", roles = "ADMIN")
    void testGetClientById() {
        String userId = "username";
        when(clientService.getClient(userId)).thenReturn(Mono.just(client));
        when(clientDtoMapper.toDto(client)).thenReturn(clientDto);

        webTestClient
                .get()
                .uri("/api/client/{id}", userId)
                .exchange()
                .expectStatus().isOk()
                .expectBody(ClientDto.class)
                .isEqualTo(clientDto);
    }

    @Test
    @WithMockUser(username = "user", roles = "ADMIN")
    void testGetAllClients(){
        when(clientService.getClients()).thenReturn(Flux.just(client));
        when(clientDtoMapper.toDto(client)).thenReturn(clientDto);

        webTestClient
                .get()
                .uri("/api/client/all")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(ClientDto.class)
                .contains(clientDto);
    }

    @Test
    @WithMockUser(username = "user", roles = "ADMIN")
    void testDeleteClient(){
        String userId = "username";
        clientService.deleteClient(userId);

        webTestClient
                .mutateWith(SecurityMockServerConfigurers.csrf())
                .delete()
                .uri("/api/client/{id}", userId)
                .exchange()
                .expectStatus().isOk();
    }
}
