package com.kata.delivery.application.rest;

import com.kata.delivery.application.dto.ClientDto;
import com.kata.delivery.application.dto.DeliveryDto;
import com.kata.delivery.application.mapper.DeliveryDtoMapper;
import com.kata.delivery.domain.exceptions.DeliveryNotFoundException;
import com.kata.delivery.domain.model.Client;
import com.kata.delivery.domain.model.Delivery;
import com.kata.delivery.domain.model.DeliveryMode;
import com.kata.delivery.domain.model.Role;
import com.kata.delivery.domain.port.api.DeliveryManagementService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

import static org.mockito.Mockito.when;


@ExtendWith(SpringExtension.class)
@WebFluxTest(DeliveryController.class)
class DeliveryControllerTest {
    @MockBean
    private DeliveryManagementService deliveryManagementService;

    @MockBean
    private DeliveryDtoMapper deliveryDtoMapper;

    @InjectMocks
    private DeliveryController deliveryController;

    @Autowired
    private WebTestClient webTestClient;

    String userId = "testUser";
    Client client = new Client("testUser", "email", "password", "adress", "phone", Role.USER);
    Delivery delivery = new Delivery("delivery","description", DeliveryMode.DELIVERY,"address",null,"22", "testUser");
    ClientDto clientDto = new ClientDto("testUser", "email", "adress", "phone");
    DeliveryDto deliveryDto = new DeliveryDto("1","delivery","description", DeliveryMode.DELIVERY,"address",null,"22", "testUser");

    @Test
    @WithMockUser(username = "user", roles = "USER")
    void testGetDeliveryById() {
        String deliveryId = "1";

        when(deliveryManagementService.getDelivery(deliveryId)).thenReturn(Mono.just(delivery));
        when(deliveryDtoMapper.toDto(delivery)).thenReturn(new DeliveryDto("1","delivery","description", DeliveryMode.DELIVERY,"address",null,"22", "testUser"));

        webTestClient.get()
                .uri("/api/delivery/{id}", deliveryId)
                .exchange()
                .expectStatus().isOk()
                .expectBody(DeliveryDto.class)
                .isEqualTo(deliveryDto);
    }

    @Test
    @WithMockUser(username = "user", roles = "USER")
    void testGetDeliveryById_shouldReturnDeliveryNotFoundException() {
        String deliveryId = "1";

        when(deliveryManagementService.getDelivery(deliveryId)).thenReturn(Mono.error(new DeliveryNotFoundException("Delivery not found")));

        webTestClient.get()
                .uri("/api/delivery/{id}", deliveryId)
                .exchange()
                .expectStatus().isNotFound()
                .expectBody(String.class)
                .isEqualTo("Delivery not found");
    }

    @Test
    @WithMockUser(username = "user", roles = "USER")
    void testGetAllDeliveries() {
        when(deliveryManagementService.getDeliveries()).thenReturn(Flux.just(delivery));
        when(deliveryDtoMapper.toDto(delivery)).thenReturn(new DeliveryDto("1","delivery","description", DeliveryMode.DELIVERY,"address",null,"22", "testUser"));

        webTestClient.get()
                .uri("/api/delivery/all")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(DeliveryDto.class)
                .contains(deliveryDto);
    }

    @Test
    @WithMockUser(username = "user", roles = "USER")
    void testGetAllDeliveries_shouldReturnEmptyList() {
        when(deliveryManagementService.getDeliveries()).thenReturn(Flux.empty());

        webTestClient.get()
                .uri("/api/delivery/all")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(DeliveryDto.class)
                .hasSize(0);
    }

    @Test
    @WithMockUser(username = "user", roles = "USER")
    void testSetDeliveryMode() {
        String deliveryId = "1";
        String deliveryMode = "DELIVERY";

        when(deliveryManagementService.setDeliveryMode(deliveryId, deliveryMode)).thenReturn(Mono.just(delivery));
        when(deliveryDtoMapper.toDto(delivery)).thenReturn(new DeliveryDto("1","delivery","description", DeliveryMode.DELIVERY,"address",null,"22", "testUser"));

        webTestClient
                .mutateWith(SecurityMockServerConfigurers.csrf())
                .put()
                .uri("/api/delivery/deliveryMode/{id}", deliveryId)
                .bodyValue(deliveryMode)
                .exchange()
                .expectStatus().isOk()
                .expectBody(DeliveryDto.class)
                .isEqualTo(deliveryDto);
    }

    @Test
    @WithMockUser(username = "user", roles = "USER")
    void testSetDeliveryMode_shouldReturnDeliveryNotFoundException() {
        String deliveryId = "1";
        String deliveryMode = "DELIVERY";

        when(deliveryManagementService.setDeliveryMode(deliveryId, deliveryMode)).thenReturn(Mono.error(new DeliveryNotFoundException("Delivery not found")));

        webTestClient
                .mutateWith(SecurityMockServerConfigurers.csrf())
                .put()
                .uri("/api/delivery/deliveryMode/{id}", deliveryId)
                .bodyValue(deliveryMode)
                .exchange()
                .expectStatus().isNotFound()
                .expectBody(String.class)
                .isEqualTo("Delivery not found");
    }

    @Test
    @WithMockUser(username = "user", roles = "USER")
    void testCreateDelivery() {
        when(deliveryManagementService.createDelivery(delivery, userId,"22")).thenReturn(Mono.just(delivery));
        when(deliveryDtoMapper.toDomain(deliveryDto)).thenReturn(delivery);
        when(deliveryDtoMapper.toDto(delivery)).thenReturn(deliveryDto);

        webTestClient
                .mutateWith(SecurityMockServerConfigurers.csrf())
                .post()
                .uri(uriBuilder -> uriBuilder.
                        path("/api/delivery")
                        .queryParam("clientId", userId)
                        .queryParam("orderId", "22")
                        .build())
                .bodyValue(deliveryDto)
                .exchange()
                .expectStatus().isOk()
                .expectBody(DeliveryDto.class)
                .isEqualTo(deliveryDto);
    }

    @Test
    @WithMockUser(username = "user", roles = "USER")
    void testSetDeliveryTimeSlot() {
        String deliveryId = "1";
        String start = "2021-08-01T10:00:00";
        LocalDateTime startDateTime = LocalDateTime.parse(start);

        when(deliveryManagementService.setDeliveryTimeSlot(deliveryId, startDateTime)).thenReturn(Mono.just(delivery));
        when(deliveryDtoMapper.toDto(delivery)).thenReturn(deliveryDto);

        webTestClient
                .mutateWith(SecurityMockServerConfigurers.csrf())
                .put()
                .uri("/api/delivery/deliveryTimeSlot/{id}", deliveryId)
                .bodyValue(start)
                .exchange()
                .expectStatus().isOk()
                .expectBody(DeliveryDto.class)
                .isEqualTo(deliveryDto);
    }

}
