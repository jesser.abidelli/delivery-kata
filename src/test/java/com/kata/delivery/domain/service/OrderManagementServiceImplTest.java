package com.kata.delivery.domain.service;

import com.kata.delivery.domain.model.Item;
import com.kata.delivery.domain.model.Order;
import com.kata.delivery.domain.port.spi.OrderPersistencePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class OrderManagementServiceImplTest {
    @Mock
    private OrderPersistencePort orderPersistencePort;

    @InjectMocks
    private OrderManagementServiceImpl orderManagementServiceImpl;

    @BeforeEach
    public void init(){
        MockitoAnnotations.openMocks(this);
    }

    Item item = new Item(1, "1");
    Item item2 = new Item(2, "2");

    Order order = new Order("1", java.util.Arrays.asList(item, item2));
    Order orderSaved = new Order("1","1", java.util.Arrays.asList(item, item2));
    Order orderSaved2 = new Order("2","2", java.util.Arrays.asList(item, item2));
    Order orderExpected = new Order("1","1", java.util.Arrays.asList(item, item2));

    @Test
    void testcreateOrder(){
        when(orderPersistencePort.save(order)).thenReturn(Mono.just(orderSaved));
        StepVerifier.create(orderManagementServiceImpl.createOrder("1",java.util.Arrays.asList(item,item2)))
                .expectNext(orderExpected)
                .verifyComplete();
    }
    @Test
    void testgetOrder(){
        when(orderPersistencePort.get("1")).thenReturn(Mono.just(orderSaved));
        StepVerifier.create(orderManagementServiceImpl.getOrder("1"))
                .expectNext(orderExpected)
                .verifyComplete();
    }
    @Test
    void testgetOrders(){
        when(orderPersistencePort.getAll()).thenReturn(Flux.just(orderSaved,orderSaved2));
        StepVerifier.create(orderManagementServiceImpl.getOrders())
                .expectNext(orderExpected,orderSaved2)
                .verifyComplete();
    }
    @Test
    void testupdateOrder(){
        when(orderPersistencePort.update(orderSaved)).thenReturn(Mono.just(orderSaved));
        StepVerifier.create(orderManagementServiceImpl.updateOrder(orderSaved))
                .expectNext(orderExpected)
                .verifyComplete();
    }
}
