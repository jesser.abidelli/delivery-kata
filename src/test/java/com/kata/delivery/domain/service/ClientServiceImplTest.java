package com.kata.delivery.domain.service;


import com.kata.delivery.domain.model.Role;
import com.kata.delivery.domain.model.Client;
import com.kata.delivery.domain.port.spi.ClientPersistencePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.Mockito.when;

class ClientServiceImplTest {
    @Mock
    private ClientPersistencePort clientPersistencePort;

    @InjectMocks
    private ClientServiceImpl clientServiceImpl;

    @BeforeEach
    public void init(){
        MockitoAnnotations.openMocks(this);
    }

    Client client = new Client("testUser","email","password","adress","phone", Role.USER);
    Client client2 = new Client("testUser2","email2","password2","adress2","phone2", Role.USER);
    @Test
    void testgetClient(){
        when(clientPersistencePort.get("1")).thenReturn(Mono.just(client));
        StepVerifier.create(clientServiceImpl.getClient("1"))
                .expectNext(client)
                .verifyComplete();
    }

    @Test
    void testcreateClient(){
        when(clientPersistencePort.save(client)).thenReturn(Mono.just(client));
        StepVerifier.create(clientServiceImpl.createClient(client))
                .expectNext(client)
                .verifyComplete();
    }

    @Test
    void testupdateClient(){
        when(clientPersistencePort.update(client)).thenReturn(Mono.just(client));
        StepVerifier.create(clientServiceImpl.updateClient(client))
                .expectNext(client)
                .verifyComplete();
    }

    @Test
    void testgetClients(){
        when(clientPersistencePort.findAll()).thenReturn(Flux.just(client, client2));
        StepVerifier.create(clientServiceImpl.getClients())
                .expectNext(client)
                .expectNext(client2)
                .verifyComplete();
    }
}
