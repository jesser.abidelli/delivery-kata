package com.kata.delivery.domain.service;

import com.kata.delivery.domain.exceptions.DeliveryNotFoundException;
import com.kata.delivery.domain.exceptions.ClientNotFoundException;
import com.kata.delivery.domain.model.Client;
import com.kata.delivery.domain.model.Delivery;
import com.kata.delivery.domain.model.DeliveryMode;
import com.kata.delivery.domain.model.Role;
import com.kata.delivery.domain.port.api.TimeSlotManagementService;
import com.kata.delivery.domain.port.spi.DeliveryCreatedPublishPort;
import com.kata.delivery.domain.port.spi.DeliveryPersistencePort;
import com.kata.delivery.domain.port.spi.ClientPersistencePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class DeliveryManagementServiceImplTest {
    @Mock
    private DeliveryPersistencePort deliveryPersistencePort;
    @Mock
    private ClientPersistencePort clientPersistencePort;
    @Mock
    private TimeSlotManagementService timeSlotManagementService;
    @Mock
    private DeliveryCreatedPublishPort deliveryCreatedPublishPort;
    @InjectMocks
    private DeliveryManagementServiceImpl deliveryManagementServiceImpl;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testCreateDeliveryWhenClientExists() {
        String userId = "testUser";
        Client client = new Client("testUser", "email", "password", "adress", "phone", Role.USER);
        Delivery delivery = new Delivery("delivery","description", DeliveryMode.DELIVERY,"address",null,"22", "testUser");

        when(clientPersistencePort.get(userId)).thenReturn(Mono.just(client));
        when(deliveryPersistencePort.save(delivery)).thenReturn(Mono.just(delivery));

        StepVerifier.create(deliveryManagementServiceImpl.createDelivery(delivery, userId))
                .expectNext(delivery)
                .verifyComplete();
    }

    @Test
    void testCreateDeliveryWhenClientDoesNotExistShouldThrowClientNotFoundException() {
        String userId = "testUser";
        Client client = new Client("testUser", "email", "password", "adress", "phone", Role.USER);
        Delivery delivery = new Delivery("delivery","description", DeliveryMode.DELIVERY,"address",null,"22", "testUser");

        when(clientPersistencePort.get(userId)).thenReturn(Mono.empty());

        StepVerifier.create(deliveryManagementServiceImpl.createDelivery(delivery, userId))
                .expectError(ClientNotFoundException.class)
                .verify();
    }

    @Test
    void testUpdateDeliveryWhenDeliveryExists() {
        String deliveryId = "1";
        Delivery delivery = new Delivery("1","delivery","description", DeliveryMode.DELIVERY,"address",null,"22",null);

        when(deliveryPersistencePort.get(deliveryId)).thenReturn(Mono.just(delivery));
        when(deliveryPersistencePort.update(delivery)).thenReturn(Mono.just(delivery));

        StepVerifier.create(deliveryManagementServiceImpl.updateDelivery(delivery))
                .expectNext(delivery)
                .verifyComplete();
    }

    @Test
    void testUpdateDeliveryWhenDeliveryDoesNotExistShouldThrowDeliveryNotFoundException() {
        String deliveryId = "1";
        Delivery delivery = new Delivery("1","delivery","description", DeliveryMode.DELIVERY,"address",null,"22",null);

        when(deliveryPersistencePort.get(deliveryId)).thenReturn(Mono.empty());

        StepVerifier.create(deliveryManagementServiceImpl.updateDelivery(delivery))
                .expectError(DeliveryNotFoundException.class)
                .verify();
    }

    @Test
    void testGetDeliveryWhenDeliveryExists() {
        String deliveryId = "1";
        Delivery delivery = new Delivery("1","delivery","description", DeliveryMode.DELIVERY,"address",null,"22",null);

        when(deliveryPersistencePort.get(deliveryId)).thenReturn(Mono.just(delivery));

        StepVerifier.create(deliveryManagementServiceImpl.getDelivery(deliveryId))
                .expectNext(delivery)
                .verifyComplete();
    }

    @Test
    void testGetDeliveryWhenDeliveryDoesNotExistShouldThrowDeliveryNotFoundException() {
        String deliveryId = "1";

        when(deliveryPersistencePort.get(deliveryId)).thenReturn(Mono.empty());

        StepVerifier.create(deliveryManagementServiceImpl.getDelivery(deliveryId))
                .expectError(DeliveryNotFoundException.class)
                .verify();
    }

    @Test
    void testGetDeliveries() {
        Delivery delivery = new Delivery("1","delivery","description", DeliveryMode.DELIVERY,"address",null,"22",null);
        Delivery delivery2 = new Delivery("2","delivery2","description2", DeliveryMode.DELIVERY,"address2",null,"22",null);

        when(deliveryPersistencePort.getAll()).thenReturn(Flux.just(delivery,delivery2));

        StepVerifier.create(deliveryManagementServiceImpl.getDeliveries())
                .expectNext(delivery)
                .expectNext(delivery2)
                .verifyComplete();
    }

    @Test
    void testSetDeliveryModeWhenDeliveryExists() {
        String deliveryId = "1";
        Delivery delivery = new Delivery("1","delivery","description", null,"address",null,"22",null);
        Delivery expectedDelivery = new Delivery("1","delivery","description", DeliveryMode.DELIVERY_ASAP,"address",null,"22",null);

        when(deliveryPersistencePort.get(deliveryId)).thenReturn(Mono.just(delivery));
        when(deliveryPersistencePort.update(delivery)).thenReturn(Mono.just(expectedDelivery));

        StepVerifier.create(deliveryManagementServiceImpl.setDeliveryMode(deliveryId, "DELIVERY_ASAP"))
                .expectNext(expectedDelivery)
                .verifyComplete();
    }

    @Test
    void testSetDeliveryModeWhenDeliveryDoesNotExistShouldThrowDeliveryNotFoundException() {
        String deliveryId = "1";
        Delivery delivery = new Delivery("1","delivery","description", null,"address",null,"22",null);

        when(deliveryPersistencePort.get(deliveryId)).thenReturn(Mono.empty());

        StepVerifier.create(deliveryManagementServiceImpl.setDeliveryMode(deliveryId, "DELIVERY_ASAP"))
                .expectError(DeliveryNotFoundException.class)
                .verify();
    }

    @Test
    void setDeliveryMode_WhenInvalidDeliveryMode_ShouldDefaultToDelivery() {
        String deliveryId = "1";
        String deliveryMode = "INVALID_MODE";
        Delivery existingDelivery = new Delivery("1", "delivery", "description", null, "address", null, "22", null);

        when(deliveryPersistencePort.get(deliveryId)).thenReturn(Mono.just(existingDelivery));
        when(deliveryPersistencePort.update(existingDelivery)).thenReturn(Mono.just(existingDelivery));

        StepVerifier.create(deliveryManagementServiceImpl.setDeliveryMode(deliveryId, deliveryMode))
                .expectNext(existingDelivery)
                .verifyComplete();
    }

    @Test
    void testSetDeliveryTimeSlotWhenDeliveryExists() {
        String deliveryId = "1";
        Delivery delivery = new Delivery("1","delivery","description", DeliveryMode.DELIVERY,"address",null,null,null);
        Delivery expectedDelivery = new Delivery("1","delivery","description", DeliveryMode.DELIVERY,"address",null,"22",null);

        when(deliveryPersistencePort.get(deliveryId)).thenReturn(Mono.just(delivery));
        when(deliveryPersistencePort.update(delivery)).thenReturn(Mono.just(delivery));
        when(timeSlotManagementService.createTimeSlot(LocalDateTime.parse("2021-06-01T10:00:00"), delivery.getDeliveryMode().name())).thenReturn(Mono.just("22"));

        StepVerifier.create(deliveryManagementServiceImpl.setDeliveryTimeSlot(deliveryId, LocalDateTime.parse("2021-06-01T10:00:00")))
                .expectNext(expectedDelivery)
                .verifyComplete();
    }

    @Test
    void testSetDeliveryTimeSlotWhenDeliveryDoesNotExistShouldThrowDeliveryNotFoundException() {
        String deliveryId = "1";
        Delivery delivery = new Delivery("1","delivery","description", DeliveryMode.DELIVERY,"address",null,null,null);

        when(deliveryPersistencePort.get(deliveryId)).thenReturn(Mono.empty());

        StepVerifier.create(deliveryManagementServiceImpl.setDeliveryTimeSlot(deliveryId, LocalDateTime.parse("2021-06-01T10:00:00")))
                .expectError(DeliveryNotFoundException.class)
                .verify();
    }
}
