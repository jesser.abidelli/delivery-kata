package com.kata.delivery.domain.service;

import com.kata.delivery.domain.model.Product;
import com.kata.delivery.domain.port.spi.ProductPersistencePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.Mockito.when;

class ProductManagementServiceImplTest {
    @Mock
    private ProductPersistencePort productPersistencePort;

    @InjectMocks
    private ProductManagementServiceImpl productManagementServiceImpl;

    @BeforeEach
    public void init(){
        MockitoAnnotations.openMocks(this);
    }

    Product productToSave = new Product( "product", 10.0, "descr");
    Product product = new Product("1", "product", 10.0, "descr");

    @Test
    void testCreateProduct(){
        when(productPersistencePort.save(productToSave)).thenReturn(Mono.just(product));
        StepVerifier.create(productManagementServiceImpl.createProduct(productToSave))
                .expectNext(product)
                .verifyComplete();
    }

    @Test
    void testUpdateProduct(){
        when(productPersistencePort.update(product)).thenReturn(Mono.just(product));
        StepVerifier.create(productManagementServiceImpl.updateProduct(product))
                .expectNext(product)
                .verifyComplete();
    }

    @Test
    void testGetProduct(){
        when(productPersistencePort.get("1")).thenReturn(Mono.just(product));
        StepVerifier.create(productManagementServiceImpl.getProduct("1"))
                .expectNext(product)
                .verifyComplete();
    }

    @Test
    void testGetProducts(){
        when(productPersistencePort.getAll()).thenReturn(Flux.just(product));
        StepVerifier.create(productManagementServiceImpl.getAllProducts())
                .expectNext(product)
                .verifyComplete();
    }

}
