package com.kata.delivery.domain.service;

import com.kata.delivery.domain.model.DeliveryMode;
import com.kata.delivery.domain.model.TimeSlot;
import com.kata.delivery.domain.port.spi.TimeSlotPersistencePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class TimeSlotManagementServiceImplTest {
    @Mock
    private TimeSlotPersistencePort timeSlotPersistencePort;

    @InjectMocks
    private TimeSlotManagementServiceImpl timeSlotManagementServiceImpl;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void createTimeSlotWhenTimeSlotExistsShouldIncrementReservationCount() {
        LocalDateTime start = LocalDateTime.now();
        DeliveryMode deliveryMode = DeliveryMode.DELIVERY;
        TimeSlot existingTimeSlot = new TimeSlot(start);
        existingTimeSlot.setId("1");
        existingTimeSlot.setDeliveryMode(deliveryMode.name());
        existingTimeSlot.setReservationCount(1);

        when(timeSlotPersistencePort.getAll()).thenReturn(Flux.just(existingTimeSlot));
        when(timeSlotPersistencePort.save(existingTimeSlot)).thenReturn(Mono.just(existingTimeSlot));

        StepVerifier.create(timeSlotManagementServiceImpl.createTimeSlot(start, deliveryMode.name()))
                .expectNext("1")
                .verifyComplete();
    }

    @Test
    void createTimeSlotWhenTimeSlotDoesNotExistShouldCreateNewTimeSlot() {
        LocalDateTime start = LocalDateTime.now();
        String deliveryMode = DeliveryMode.DELIVERY.name();


        when(timeSlotPersistencePort.getAll()).thenReturn(Flux.empty());
        when(timeSlotPersistencePort.save(any())).thenAnswer(invocation -> {
            TimeSlot createdTimeSlot = invocation.getArgument(0);
            createdTimeSlot.setId("1");
            return Mono.just(createdTimeSlot);
        });

        StepVerifier.create(timeSlotManagementServiceImpl.createTimeSlot(start, deliveryMode))
                .expectNext("1")
                .verifyComplete();
    }
}
