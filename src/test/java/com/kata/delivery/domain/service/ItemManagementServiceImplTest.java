package com.kata.delivery.domain.service;

import com.kata.delivery.domain.model.Item;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import reactor.test.StepVerifier;

class ItemManagementServiceImplTest {
    @InjectMocks
    private ItemManagementServiceImpl itemManagementServiceImpl;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    Item item = new Item(10, "1");
    @Test
    void testCreateItem() {
        StepVerifier.create(itemManagementServiceImpl.createItem(10, "1"))
                .expectNextMatches(item -> item.getQuantity() == 10 && item.getProductId().equals("1"))
                .verifyComplete();
    }
}
