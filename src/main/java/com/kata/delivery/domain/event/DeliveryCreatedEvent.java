package com.kata.delivery.domain.event;

import com.kata.delivery.domain.model.Delivery;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeliveryCreatedEvent {
    Delivery delivery;
    LocalDateTime publishedAt;
}