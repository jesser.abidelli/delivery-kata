package com.kata.delivery.domain.service;

import com.kata.delivery.domain.model.Item;
import com.kata.delivery.domain.port.api.ItemManagementService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Service
public class ItemManagementServiceImpl implements ItemManagementService{
    @Override
    public Mono<Item> createItem(int quantity, String productId) {
        return Mono.just(new Item(UUID.randomUUID().toString(),quantity,productId));
    }
}