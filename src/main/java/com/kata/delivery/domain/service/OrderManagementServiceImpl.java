package com.kata.delivery.domain.service;

import com.kata.delivery.domain.model.Item;
import com.kata.delivery.domain.model.Order;
import com.kata.delivery.domain.port.api.OrderManagementService;
import com.kata.delivery.domain.port.spi.OrderPersistencePort;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
public class OrderManagementServiceImpl implements OrderManagementService {
    private final OrderPersistencePort orderPersistencePort;

    public OrderManagementServiceImpl(OrderPersistencePort orderPersistencePort) {
        this.orderPersistencePort = orderPersistencePort;
    }
    @Override
    public Mono<Order> createOrder(String clientId, List<Item> items) {
        Order order = new Order(clientId, items);
        return orderPersistencePort.save(order);
    }
    @Override
    public Mono<Order> updateOrder(Order order) {
        return orderPersistencePort.update(order);
    }
    @Override
    public Mono<Void> deleteOrder(String id) {
        return orderPersistencePort.delete(id);
    }
    @Override
    public Mono<Order> getOrder(String id) {
        return orderPersistencePort.get(id);
    }
    @Override
    public Flux<Order> getOrders() {
        return orderPersistencePort.getAll();
    }
}
