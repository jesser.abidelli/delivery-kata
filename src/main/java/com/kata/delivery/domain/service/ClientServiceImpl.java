package com.kata.delivery.domain.service;

import com.kata.delivery.domain.model.Client;
import com.kata.delivery.domain.model.Role;
import com.kata.delivery.domain.port.api.ClientService;
import com.kata.delivery.domain.port.spi.ClientPersistencePort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ClientServiceImpl implements ClientService {
    private final ClientPersistencePort clientPersistencePort;

    private final BCryptPasswordEncoder passwordEncoder;

    public ClientServiceImpl(ClientPersistencePort clientPersistencePort) {
        this.clientPersistencePort = clientPersistencePort;
        this.passwordEncoder = new BCryptPasswordEncoder();
    }
    @Override
    public Mono<Client> createClient(Client client) {
        client.setPassword(passwordEncoder.encode(client.getPassword()));
        if (client.getRole() == null) {
            client.setRole(Role.USER);
        }
        return clientPersistencePort.save(client);
    }

    @Override
    public Mono<Client> getClient(String id) {
        return clientPersistencePort.get(id);
    }

    @Override
    public Mono<Client> updateClient(Client client) {
        return clientPersistencePort.update(client);
    }

    @Override
    public void deleteClient(String id) {
        clientPersistencePort.delete(id);
    }

    @Override
    public Flux<Client> getClients() {
        return clientPersistencePort.findAll();
    }
}