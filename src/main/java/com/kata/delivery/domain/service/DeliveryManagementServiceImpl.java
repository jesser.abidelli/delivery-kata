package com.kata.delivery.domain.service;

import com.kata.delivery.domain.exceptions.DeliveryNotFoundException;
import com.kata.delivery.domain.exceptions.OrderNotFoundException;
import com.kata.delivery.domain.exceptions.ClientNotFoundException;
import com.kata.delivery.domain.model.Delivery;
import com.kata.delivery.domain.model.DeliveryMode;
import com.kata.delivery.domain.port.api.DeliveryEventsPublishService;
import com.kata.delivery.domain.port.api.DeliveryManagementService;
import com.kata.delivery.domain.port.api.TimeSlotManagementService;
import com.kata.delivery.domain.port.spi.DeliveryCreatedPublishPort;
import com.kata.delivery.domain.port.spi.DeliveryPersistencePort;
import com.kata.delivery.domain.port.spi.OrderPersistencePort;
import com.kata.delivery.domain.port.spi.ClientPersistencePort;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

@Service
public class DeliveryManagementServiceImpl implements DeliveryManagementService {

    private final DeliveryPersistencePort deliveryPersistencePort;

    private final ClientPersistencePort clientPersistencePort;
    private final OrderPersistencePort orderPersistencePort;

    private final TimeSlotManagementService timeSlotManagementService;

    private final DeliveryEventsPublishService deliveryEventsPublishService;

    public DeliveryManagementServiceImpl(DeliveryPersistencePort deliveryPersistencePort, ClientPersistencePort clientPersistencePort, OrderPersistencePort orderPersistencePort, TimeSlotManagementService timeSlotManagementService, DeliveryCreatedPublishPort deliveryCreatedPublishPort) {
        this.deliveryPersistencePort = deliveryPersistencePort;
        this.clientPersistencePort = clientPersistencePort;
        this.orderPersistencePort = orderPersistencePort;
        this.timeSlotManagementService = timeSlotManagementService;
        this.deliveryEventsPublishService = new DeliveryEventsPublishSerivceImpl(deliveryCreatedPublishPort);
    }
    @Override
    public Mono<Delivery> createDelivery(Delivery delivery, String clientId){

        return clientPersistencePort.get(clientId)
                .switchIfEmpty(Mono.error(new ClientNotFoundException("Client not found")))
                .map(client -> {
                    delivery.setClientId(clientId);
                    return delivery;
                })
                .flatMap(deliveryPersistencePort::save);
    }
    @Override
    public Mono<Delivery> createDelivery(Delivery delivery, String clientId, String orderId) {
        return clientPersistencePort.get(clientId)
                .switchIfEmpty(Mono.error(new ClientNotFoundException("Client not found")))
                .flatMap(client -> orderPersistencePort.get(orderId)
                        .switchIfEmpty(Mono.error(new OrderNotFoundException("Order not found")))
                        .map(order -> {
                            delivery.setClientId(clientId);
                            delivery.setOrder(order);
                            return delivery;
                        }))
                .flatMap(deliveryPersistencePort::save);
    }
    @Override
    public Mono<Delivery> updateDelivery(Delivery delivery){
        return deliveryPersistencePort.get(delivery.getId())
                .switchIfEmpty(Mono.error(new DeliveryNotFoundException("Delivery not found")))
                .flatMap(deliveryPersistencePort::update);
    }
    @Override
    public void deleteDelivery(String id){
        deliveryPersistencePort.delete(id);
    }
    @Override
    public Mono<Delivery> getDelivery(String id){
        return deliveryPersistencePort.get(id)
                .switchIfEmpty(Mono.error(new DeliveryNotFoundException("Delivery not found")));
    }
    @Override
    public Flux<Delivery> getDeliveries(){
        return deliveryPersistencePort.getAll();
    }
    @Override
    public Mono<Delivery> setDeliveryMode(String id, String deliveryMode){
        return deliveryPersistencePort.get(id)
                .switchIfEmpty(Mono.error(new DeliveryNotFoundException("Delivery not found")))
                .map(delivery -> {
                    DeliveryMode mode;
                    try {
                        mode = DeliveryMode.valueOf(deliveryMode);
                    } catch (IllegalArgumentException e) {
                        mode = DeliveryMode.DELIVERY;
                    }
                    delivery.setDeliveryMode(mode);
                    return delivery;
                })
                .flatMap(deliveryPersistencePort::update);
    }

    @Override
    public Mono<Delivery> setDeliveryTimeSlot(String id, LocalDateTime start) {
        return deliveryPersistencePort.get(id)
                .switchIfEmpty(Mono.error(new DeliveryNotFoundException("Delivery not found")))
                .flatMap(delivery -> timeSlotManagementService.createTimeSlot(start, delivery.getDeliveryMode().name())
                        .flatMap(timeSlotId -> {
                            delivery.setTimeSlotId(timeSlotId);
                            return deliveryPersistencePort.update(delivery)
                                    .doOnSuccess(deliveryEventsPublishService::publishEventCreatedEvent);
                        }));
    }
}
