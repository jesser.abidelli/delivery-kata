package com.kata.delivery.domain.service;

import com.kata.delivery.domain.event.DeliveryCreatedEvent;
import com.kata.delivery.domain.model.Delivery;
import com.kata.delivery.domain.port.api.DeliveryEventsPublishService;
import com.kata.delivery.domain.port.spi.DeliveryCreatedPublishPort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class DeliveryEventsPublishSerivceImpl implements DeliveryEventsPublishService {
    private final DeliveryCreatedPublishPort deliveryCreatedPublishPort;
    public DeliveryEventsPublishSerivceImpl(DeliveryCreatedPublishPort deliveryCreatedPublishPort) {
        this.deliveryCreatedPublishPort = deliveryCreatedPublishPort;
    }
    @Override
    public String publishEventCreatedEvent(Delivery delivery) {
        DeliveryCreatedEvent deliveryCreatedEvent = new DeliveryCreatedEvent(delivery, LocalDateTime.now());
        deliveryCreatedPublishPort.publish(deliveryCreatedEvent);
        return "Delivery published event sent successfully";
    }
}
