package com.kata.delivery.domain.service;

import com.kata.delivery.domain.model.Product;
import com.kata.delivery.domain.port.api.ProductManagementService;
import com.kata.delivery.domain.port.spi.ProductPersistencePort;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Service
public class ProductManagementServiceImpl implements ProductManagementService {

    private final ProductPersistencePort productPersistencePort;

    public ProductManagementServiceImpl(ProductPersistencePort productPersistencePort) {
        this.productPersistencePort = productPersistencePort;
    }
    @Override
    public Mono<Product> createProduct(Product product) {
        return productPersistencePort.save(product);
    }
    @Override
    public Mono<Product> updateProduct(Product product) {
        return productPersistencePort.update(product);
    }
    @Override
    public void deleteProduct(String id) {
        productPersistencePort.delete(id);
    }
    @Override
    public Mono<Product> getProduct(String id) {
        return productPersistencePort.get(id);
    }
    @Override
    public Flux<Product> getAllProducts() {
        return productPersistencePort.getAll();
    }

}
