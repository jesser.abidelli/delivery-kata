package com.kata.delivery.domain.service;

import com.kata.delivery.domain.model.DeliveryEnum;
import com.kata.delivery.domain.model.TimeSlot;
import com.kata.delivery.domain.port.api.TimeSlotManagementService;
import com.kata.delivery.domain.port.spi.TimeSlotPersistencePort;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

@Service
public class TimeSlotManagementServiceImpl implements TimeSlotManagementService {
    private final TimeSlotPersistencePort timeSlotPersistencePort;

    public TimeSlotManagementServiceImpl(TimeSlotPersistencePort timeSlotPersistencePort) {
        this.timeSlotPersistencePort = timeSlotPersistencePort;
    }

    @Override
    public Mono<String> createTimeSlot(LocalDateTime start, String deliveryMode) {
        return timeSlotPersistencePort.getAll()
                .filter(timeSlot -> timeSlot.getStart().equals(start) && timeSlot.getDeliveryMode().equals(deliveryMode))
                .next()
                .flatMap(timeSlotFound -> {
                    timeSlotFound.setReservationCount(timeSlotFound.getReservationCount() + 1);
                    return timeSlotPersistencePort.save(timeSlotFound).map(TimeSlot::getId);
                })
                .switchIfEmpty(Mono.defer(() -> {
                    TimeSlot timeSlot = new TimeSlot(start);
                    LocalDateTime end = start.plusMinutes(DeliveryEnum.getSlotDuration(deliveryMode));
                    timeSlot.setEnd(end);
                    timeSlot.setDeliveryMode(deliveryMode);
                    timeSlot.setReservationCount(timeSlot.getReservationCount() + 1);
                    return timeSlotPersistencePort.save(timeSlot).map(TimeSlot::getId);
                }));
    }
}
