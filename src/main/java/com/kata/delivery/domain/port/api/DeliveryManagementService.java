package com.kata.delivery.domain.port.api;

import com.kata.delivery.domain.model.Delivery;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.List;

public interface DeliveryManagementService {
    Mono<Delivery> createDelivery(Delivery delivery, String clientId);
    Mono<Delivery> updateDelivery(Delivery delivery);
    void deleteDelivery(String id);
    Mono<Delivery> getDelivery(String id);
    Flux<Delivery> getDeliveries();
    Mono<Delivery> setDeliveryMode(String id, String deliveryMode);
    Mono<Delivery> setDeliveryTimeSlot(String id, LocalDateTime start);
    Mono<Delivery> createDelivery(Delivery delivery, String clientId, String orderId);
}