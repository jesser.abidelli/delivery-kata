package com.kata.delivery.domain.port.spi;

import com.kata.delivery.domain.event.DeliveryCreatedEvent;

public interface DeliveryCreatedPublishPort {
    void publish(DeliveryCreatedEvent event);
}
