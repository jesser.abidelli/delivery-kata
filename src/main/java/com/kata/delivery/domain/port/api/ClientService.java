package com.kata.delivery.domain.port.api;

import com.kata.delivery.domain.model.Client;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ClientService {
    Mono<Client> createClient(Client client);
    Mono<Client> getClient(String id);
    Flux<Client> getClients();
    Mono<Client> updateClient(Client client);
    void deleteClient(String id);
}
