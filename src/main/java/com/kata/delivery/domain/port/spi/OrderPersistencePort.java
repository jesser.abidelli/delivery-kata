package com.kata.delivery.domain.port.spi;

import com.kata.delivery.domain.model.Order;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface OrderPersistencePort {
    public Mono<Order> save(Order order);
    public Mono<Order> update(Order order);
    public Mono<Void> delete(String id);
    public Mono<Order> get(String id);
    public Flux<Order> getAll();
}
