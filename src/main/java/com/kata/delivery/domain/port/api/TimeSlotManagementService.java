package com.kata.delivery.domain.port.api;


import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

public interface TimeSlotManagementService {
    Mono<String> createTimeSlot(LocalDateTime start, String deliveryMode);
}
