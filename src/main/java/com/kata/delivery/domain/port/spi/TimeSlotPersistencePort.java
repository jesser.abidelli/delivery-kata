package com.kata.delivery.domain.port.spi;

import com.kata.delivery.domain.model.TimeSlot;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;


public interface TimeSlotPersistencePort {
    Mono<TimeSlot> save(TimeSlot timeSlot);
    Mono<TimeSlot> update(TimeSlot timeSlot);
    Mono<TimeSlot> get(String id);
    Flux<TimeSlot> getAll();
}
