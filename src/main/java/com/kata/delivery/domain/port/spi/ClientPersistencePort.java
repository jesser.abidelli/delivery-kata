package com.kata.delivery.domain.port.spi;

import com.kata.delivery.domain.model.Client;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ClientPersistencePort {
    Mono<Client> save(Client client);
    Flux<Client> findAll();
    Mono<Client> get(String id);
    Mono<Client> update(Client client);
    void delete(String id);
}
