package com.kata.delivery.domain.port.spi;

import com.kata.delivery.domain.model.Delivery;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface DeliveryPersistencePort {
    Mono<Delivery> save(Delivery delivery);
    Mono<Delivery> update(Delivery delivery);
    void delete(String id);
    Mono<Delivery> get(String id);
    Flux<Delivery> getAll();
}
