package com.kata.delivery.domain.port.api;

import com.kata.delivery.domain.model.Item;
import reactor.core.publisher.Mono;

import java.util.List;

public interface ItemManagementService {
    Mono<Item> createItem(int quantity, String productId);
}