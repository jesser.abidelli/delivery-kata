package com.kata.delivery.domain.port.api;

import com.kata.delivery.domain.model.Product;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface ProductManagementService{
    Mono<Product> createProduct(Product product);
    Mono<Product> updateProduct(Product product);
    void deleteProduct(String id);
    Mono<Product> getProduct(String id);
    Flux<Product> getAllProducts();
}
