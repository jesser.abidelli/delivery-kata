package com.kata.delivery.domain.port.spi;

import com.kata.delivery.domain.model.Product;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface ProductPersistencePort {
    Mono<Product> save(Product product);
    Mono<Product> update(Product product);
    void delete(String id);
    Mono<Product> get(String id);
    Flux<Product> getAll();
}
