package com.kata.delivery.domain.port.api;

import com.kata.delivery.domain.model.Delivery;

public interface DeliveryEventsPublishService {
    String publishEventCreatedEvent(final Delivery delivery);
}
