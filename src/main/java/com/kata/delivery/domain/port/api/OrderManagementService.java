package com.kata.delivery.domain.port.api;

import com.kata.delivery.domain.model.Item;
import com.kata.delivery.domain.model.Order;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface OrderManagementService {
    Mono<Order> createOrder(String clientId, List<Item> items);
    Mono<Order> updateOrder(Order order);
    Mono<Void> deleteOrder(String id);
    Mono<Order> getOrder(String id);
    Flux<Order> getOrders();
}
