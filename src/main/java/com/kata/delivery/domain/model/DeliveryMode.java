package com.kata.delivery.domain.model;

public enum DeliveryMode {
    DRIVE, DELIVERY, DELIVERY_TODAY, DELIVERY_ASAP
}
