package com.kata.delivery.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TimeSlot {
    private String id;
    private LocalDateTime start;
    private LocalDateTime end;
    private Integer reservationCount=0;
    private String deliveryMode;
    public TimeSlot(LocalDateTime start) {
        this.start = start;
    }
}
