package com.kata.delivery.domain.model;

public enum DeliveryEnum {
    DRIVE("DRIVE",15),
    DELIVERY("DELIVERY",30),
    DELIVERY_TODAY("DELIVERY_TODAY",50),
    DELIVERY_ASAP("DELIVERY_ASAP",60);

    private final String mode;
    private final int slotDuration;

    DeliveryEnum(String mode,int slotDuration){
        this.mode = mode;
        this.slotDuration = slotDuration;
    }

    public String getMode(){
        return this.mode;
    }

    public int getSlotDuration(){
        return this.slotDuration;
    }
    public static int getSlotDuration(String mode){
        for(DeliveryEnum deliveryMode : DeliveryEnum.values()){
            if(deliveryMode.getMode().equals(mode)){
                return deliveryMode.getSlotDuration();
            }
        }
        return 0;
    }
}