package com.kata.delivery.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Item {
    private String id;
    private int quantity;
    private String productId;

    public Item(int quantity, String productId) {
        this.quantity = quantity;
        this.productId = productId;
    }
}