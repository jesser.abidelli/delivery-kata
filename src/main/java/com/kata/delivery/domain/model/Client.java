package com.kata.delivery.domain.model;

import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Client {
    @NotEmpty(message = "username should not be empty")
    private String username;
    private String email;
    private String password;
    private String address;
    private String phone;
    private Role role;
}