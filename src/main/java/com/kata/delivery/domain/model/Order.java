package com.kata.delivery.domain.model;

import java.util.List;

public record Order(String id, String clientId, List<Item> items) {
    public Order(String clientId, List<Item> items) {
        this(null, clientId, items);
    }
}
