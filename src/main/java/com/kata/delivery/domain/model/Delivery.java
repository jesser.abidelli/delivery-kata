package com.kata.delivery.domain.model;



import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Delivery {
    private String id;
    @NotEmpty(message = "name should not be empty")
    private String name;
    private String description;
    private DeliveryMode deliveryMode;
    private String address;
    private Order order;
    private String timeSlotId;
    private String clientId;

    public Delivery(String name, String description, DeliveryMode deliveryMode, String address, Order order, String timeSlotId, String clientId) {
        this.name = name;
        this.description = description;
        this.deliveryMode = deliveryMode == null ? DeliveryMode.DELIVERY : deliveryMode;
        this.address = address;
        this.order = order;
        this.timeSlotId = timeSlotId;
        this.clientId = clientId;
    }

}