package com.kata.delivery.domain.model;


public record Product(String id, String name, Double unitPrice, String description) {
    public Product(String name, Double unitPrice, String description) {
        this(null, name, unitPrice, description);
    }
}