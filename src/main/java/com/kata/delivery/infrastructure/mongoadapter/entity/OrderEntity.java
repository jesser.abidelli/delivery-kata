package com.kata.delivery.infrastructure.mongoadapter.entity;

import com.kata.delivery.domain.model.Item;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
public record OrderEntity(String id, String clientId, List<Item> items) {
    public OrderEntity(String clientId, List<Item> items) {
        this(null, clientId, items);
    }
}