package com.kata.delivery.infrastructure.mongoadapter;

import com.kata.delivery.domain.model.Client;
import com.kata.delivery.domain.port.spi.ClientPersistencePort;
import com.kata.delivery.infrastructure.mongoadapter.mapper.ClientEntityMapper;
import com.kata.delivery.infrastructure.mongoadapter.repository.ClientEntityRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ClientMongoAdapter implements ClientPersistencePort {
    private final ClientEntityRepository clientEntityRepository;
    private final ClientEntityMapper clientEntityMapper;

    public ClientMongoAdapter(ClientEntityRepository clientEntityRepository, ClientEntityMapper clientEntityMapper) {
        this.clientEntityRepository = clientEntityRepository;
        this.clientEntityMapper = clientEntityMapper;
    }

    @Override
    public Mono<Client> save(Client client) {
        return clientEntityRepository.save(clientEntityMapper.toEntity(client))
                .map(clientEntityMapper::toDomain);
    }

    @Override
    public Mono<Client> get(String id) {
        return clientEntityRepository.findById(id)
                .map(clientEntityMapper::toDomain);
    }

    @Override
    public Mono<Client> update(Client client) {
        return clientEntityRepository.save(clientEntityMapper.toEntity(client))
                .map(clientEntityMapper::toDomain);
    }

    @Override
    public void delete(String id) {
        clientEntityRepository.deleteById(id);
    }

    @Override
    public Flux<Client> findAll() {
        return clientEntityRepository.findAll()
                .map(clientEntityMapper::toDomain);
    }
}

