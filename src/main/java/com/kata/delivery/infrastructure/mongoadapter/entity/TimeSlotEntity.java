package com.kata.delivery.infrastructure.mongoadapter.entity;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document
public record TimeSlotEntity(
        @Id String id,
        LocalDateTime start,
        LocalDateTime end,
        Integer reservationCount,
        String deliveryMode
) {
}
