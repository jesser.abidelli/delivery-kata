package com.kata.delivery.infrastructure.mongoadapter.entity;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public record ProductEntity(String id, String name, Double unitPrice, String description) {
    public ProductEntity(String name, Double unitPrice, String description) {
        this(null, name, unitPrice, description);
    }
}