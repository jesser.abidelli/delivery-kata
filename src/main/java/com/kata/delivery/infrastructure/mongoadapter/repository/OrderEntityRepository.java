package com.kata.delivery.infrastructure.mongoadapter.repository;

import com.kata.delivery.infrastructure.mongoadapter.entity.OrderEntity;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface OrderEntityRepository extends ReactiveMongoRepository<OrderEntity,String> {
}
