package com.kata.delivery.infrastructure.mongoadapter.repository;

import com.kata.delivery.infrastructure.mongoadapter.entity.TimeSlotEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface TimeSlotEntityRepository extends ReactiveMongoRepository<TimeSlotEntity,String> {
}
