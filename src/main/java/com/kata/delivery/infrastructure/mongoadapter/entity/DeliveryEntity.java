package com.kata.delivery.infrastructure.mongoadapter.entity;

import com.kata.delivery.domain.model.DeliveryMode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;


@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeliveryEntity {
    @Id
    private String id;
    private String name;
    private String description;
    private DeliveryMode deliveryMode;
    private String address;
    private OrderEntity order;
    private String timeSlotId;
    private String clientId;
}