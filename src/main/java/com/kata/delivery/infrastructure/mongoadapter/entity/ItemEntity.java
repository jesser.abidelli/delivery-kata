package com.kata.delivery.infrastructure.mongoadapter.entity;

public record ItemEntity(String id, int quantity, String productId) {
}