package com.kata.delivery.infrastructure.mongoadapter.mapper;


import com.kata.delivery.domain.model.Order;
import com.kata.delivery.infrastructure.mongoadapter.entity.OrderEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OrderEntityMapper {
    public OrderEntity toEntity(Order order);
    public Order toDomain(OrderEntity orderEntity);
}
