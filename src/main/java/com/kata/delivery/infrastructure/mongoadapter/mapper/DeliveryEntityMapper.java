package com.kata.delivery.infrastructure.mongoadapter.mapper;

import com.kata.delivery.domain.model.Delivery;
import com.kata.delivery.infrastructure.mongoadapter.entity.DeliveryEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DeliveryEntityMapper {
    Delivery toDomain(DeliveryEntity deliveryEntity);
    DeliveryEntity toEntity(Delivery delivery);
}
