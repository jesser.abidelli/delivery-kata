package com.kata.delivery.infrastructure.mongoadapter.entity;

import com.kata.delivery.domain.model.Role;
import org.springframework.data.annotation.Id;

public record ClientEntity(
        @Id String username,
        String email,
        String password,
        String address,
        String phone,
        Role role
) {
}