package com.kata.delivery.infrastructure.mongoadapter;

import com.kata.delivery.domain.model.Delivery;
import com.kata.delivery.domain.port.spi.DeliveryPersistencePort;
import com.kata.delivery.infrastructure.mongoadapter.mapper.DeliveryEntityMapper;
import com.kata.delivery.infrastructure.mongoadapter.repository.DeliveryEntityRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Service
public class DeliveryMongoAdapter implements DeliveryPersistencePort {

    private final DeliveryEntityRepository deliveryEntityRepository;
    private final DeliveryEntityMapper deliveryEntityMapper;
    public DeliveryMongoAdapter(DeliveryEntityRepository deliveryEntityRepository, DeliveryEntityMapper deliveryEntityMapper) {
        this.deliveryEntityRepository = deliveryEntityRepository;
        this.deliveryEntityMapper = deliveryEntityMapper;
    }
    public Mono<Delivery> save(Delivery delivery){
        return deliveryEntityRepository.save(deliveryEntityMapper.toEntity(delivery))
                .map(deliveryEntityMapper::toDomain);
    }
    public Mono<Delivery> update(Delivery delivery){
        return deliveryEntityRepository.save(deliveryEntityMapper.toEntity(delivery))
                .map(deliveryEntityMapper::toDomain);
    }
    public void delete(String id){
        deliveryEntityRepository.deleteById(id);
    }
    public Mono<Delivery> get(String id){
        return deliveryEntityRepository.findById(id)
                .map(deliveryEntityMapper::toDomain);
    }
    public Flux<Delivery> getAll(){
        return deliveryEntityRepository.findAll()
                .map(deliveryEntityMapper::toDomain);
    }
}