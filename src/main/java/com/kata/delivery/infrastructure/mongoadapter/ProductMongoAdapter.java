package com.kata.delivery.infrastructure.mongoadapter;

import com.kata.delivery.domain.model.Product;
import com.kata.delivery.domain.port.spi.ProductPersistencePort;
import com.kata.delivery.infrastructure.mongoadapter.mapper.ProductEntityMapper;
import com.kata.delivery.infrastructure.mongoadapter.repository.ProductEntityRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Service
public class ProductMongoAdapter implements ProductPersistencePort {

    private final ProductEntityRepository productEntityRepository;

    private final ProductEntityMapper productEntityMapper;

    public ProductMongoAdapter(ProductEntityRepository productEntityRepository, ProductEntityMapper productEntityMapper) {
        this.productEntityRepository = productEntityRepository;
        this.productEntityMapper = productEntityMapper;
    }
    public Mono<Product> save(Product product) {
        return productEntityRepository.save(productEntityMapper.toEntity(product))
                .map(productEntityMapper::toDomain);
    }

    public Mono<Product> update(Product product) {
        return productEntityRepository.save(productEntityMapper.toEntity(product))
                .map(productEntityMapper::toDomain);
    }

    public void delete(String id) {
        productEntityRepository.deleteById(id);
    }

    public Mono<Product> get(String id) {
        return productEntityRepository.findById(id)
                .map(productEntityMapper::toDomain);
    }

    public Flux<Product> getAll() {
        return productEntityRepository.findAll()
                .map(productEntityMapper::toDomain);
    }
}
