package com.kata.delivery.infrastructure.mongoadapter.repository;

import com.kata.delivery.infrastructure.mongoadapter.entity.ProductEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface ProductEntityRepository extends ReactiveMongoRepository<ProductEntity, String> {
}
