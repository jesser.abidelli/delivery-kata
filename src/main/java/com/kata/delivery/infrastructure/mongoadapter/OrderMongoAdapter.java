package com.kata.delivery.infrastructure.mongoadapter;

import com.kata.delivery.domain.model.Order;
import com.kata.delivery.domain.port.spi.OrderPersistencePort;
import com.kata.delivery.infrastructure.mongoadapter.mapper.OrderEntityMapper;
import com.kata.delivery.infrastructure.mongoadapter.repository.OrderEntityRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Service
public class OrderMongoAdapter implements OrderPersistencePort {

    private final OrderEntityRepository orderEntityRepository;

    private final OrderEntityMapper orderEntityMapper;

    public OrderMongoAdapter(OrderEntityRepository orderEntityRepository, OrderEntityMapper orderEntityMapper) {
        this.orderEntityRepository = orderEntityRepository;
        this.orderEntityMapper = orderEntityMapper;
    }

    @Override
    public Mono<Order> save(Order order) {
        return orderEntityRepository.save(orderEntityMapper.toEntity(order))
                .map(orderEntityMapper::toDomain);
    }

    @Override
    public Mono<Order> update(Order order) {
        return orderEntityRepository.save(orderEntityMapper.toEntity(order))
                .map(orderEntityMapper::toDomain);
    }

    @Override
    public Mono<Void> delete(String id) {
        return orderEntityRepository.deleteById(id);
    }

    @Override
    public Mono<Order> get(String id) {
        return orderEntityRepository.findById(id)
                .map(orderEntityMapper::toDomain);
    }

    @Override
    public Flux<Order> getAll() {
        return orderEntityRepository.findAll()
                .map(orderEntityMapper::toDomain);
    }
}