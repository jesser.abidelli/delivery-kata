package com.kata.delivery.infrastructure.mongoadapter;

import com.kata.delivery.domain.model.TimeSlot;
import com.kata.delivery.domain.port.spi.TimeSlotPersistencePort;
import com.kata.delivery.infrastructure.mongoadapter.mapper.TimeSlotEntityMapper;
import com.kata.delivery.infrastructure.mongoadapter.repository.TimeSlotEntityRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Service
public class TimeSlotMongoAdapter implements TimeSlotPersistencePort {
    private final TimeSlotEntityRepository timeSlotEntityRepository;
    private final TimeSlotEntityMapper timeSlotEntityMapper;

    public TimeSlotMongoAdapter(TimeSlotEntityRepository timeSlotEntityRepository, TimeSlotEntityMapper timeSlotEntityMapper) {
        this.timeSlotEntityRepository = timeSlotEntityRepository;
        this.timeSlotEntityMapper = timeSlotEntityMapper;
    }


    @Override
    public Mono<TimeSlot> save(TimeSlot timeSlot) {
        return timeSlotEntityRepository.save(timeSlotEntityMapper.toEntity(timeSlot))
                .map(timeSlotEntityMapper::toDomain);
    }

    @Override
    public Mono<TimeSlot> update(TimeSlot timeSlot) {
        return timeSlotEntityRepository.save(timeSlotEntityMapper.toEntity(timeSlot))
                .map(timeSlotEntityMapper::toDomain);
    }

    @Override
    public Mono<TimeSlot> get(String id) {
        return timeSlotEntityRepository.findById(id)
                .map(timeSlotEntityMapper::toDomain);
    }

    @Override
    public Flux<TimeSlot> getAll() {
        return timeSlotEntityRepository.findAll()
                .map(timeSlotEntityMapper::toDomain);
    }
}