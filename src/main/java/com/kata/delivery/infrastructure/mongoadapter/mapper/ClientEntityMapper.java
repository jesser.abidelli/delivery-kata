package com.kata.delivery.infrastructure.mongoadapter.mapper;

import com.kata.delivery.domain.model.Client;
import com.kata.delivery.infrastructure.mongoadapter.entity.ClientEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ClientEntityMapper {
    Client toDomain(ClientEntity clientEntity);
    ClientEntity toEntity(Client client);
}
