package com.kata.delivery.infrastructure.mongoadapter.mapper;


import com.kata.delivery.domain.model.Product;
import com.kata.delivery.infrastructure.mongoadapter.entity.ProductEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductEntityMapper {
    Product toDomain(ProductEntity productEntity);
    ProductEntity toEntity(Product product);
}
