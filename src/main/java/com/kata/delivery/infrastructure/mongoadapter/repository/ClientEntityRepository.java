package com.kata.delivery.infrastructure.mongoadapter.repository;

import com.kata.delivery.infrastructure.mongoadapter.entity.ClientEntity;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface ClientEntityRepository extends ReactiveMongoRepository<ClientEntity,String> {
    Mono<ClientEntity> findByUsername(String username);
}
