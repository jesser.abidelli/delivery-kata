package com.kata.delivery.infrastructure.mongoadapter.mapper;

import com.kata.delivery.domain.model.Item;
import com.kata.delivery.infrastructure.mongoadapter.entity.ItemEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ItemEntityMapper {
    Item toDomain(ItemEntity itemEntity);
    ItemEntity toEntity(Item item);
}
