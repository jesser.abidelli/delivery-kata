package com.kata.delivery.infrastructure.mongoadapter.repository;

import com.kata.delivery.infrastructure.mongoadapter.entity.DeliveryEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface DeliveryEntityRepository extends ReactiveMongoRepository<DeliveryEntity,String> {
}
