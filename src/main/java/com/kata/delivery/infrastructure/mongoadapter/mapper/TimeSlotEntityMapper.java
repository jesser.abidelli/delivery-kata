package com.kata.delivery.infrastructure.mongoadapter.mapper;

import com.kata.delivery.domain.model.TimeSlot;
import com.kata.delivery.infrastructure.mongoadapter.entity.TimeSlotEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TimeSlotEntityMapper {
    TimeSlot toDomain(TimeSlotEntity timeSlotEntity);

    TimeSlotEntity toEntity(TimeSlot timeSlot);
}
