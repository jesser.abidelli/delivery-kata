package com.kata.delivery.infrastructure.kafkaadapter.publisher;

import com.kata.delivery.domain.event.DeliveryCreatedEvent;
import com.kata.delivery.domain.port.spi.DeliveryCreatedPublishPort;
import com.kata.delivery.infrastructure.kafkaadapter.dto.DeliveryCreatedEventDto;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class DerliveryCreatedPublisher implements DeliveryCreatedPublishPort {

    private final KafkaTemplate<String, Object> kafkaTemplate;

    public DerliveryCreatedPublisher(KafkaTemplate<String, Object> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }
    @Override
    public void publish(DeliveryCreatedEvent event) {
        DeliveryCreatedEventDto deliveryCreatedEventDto = new DeliveryCreatedEventDto(event);
        switch (event.getDelivery().getDeliveryMode()) {
            case DRIVE:
                kafkaTemplate.send("delivery-drive",event.getDelivery().getId() ,deliveryCreatedEventDto);
                break;
            case DELIVERY:
                kafkaTemplate.send("delivery-delivery",event.getDelivery().getId(), deliveryCreatedEventDto);
                break;
            case DELIVERY_TODAY:
                kafkaTemplate.send("delivery-delivery-today",event.getDelivery().getId(), deliveryCreatedEventDto);
                break;
            case DELIVERY_ASAP:
                kafkaTemplate.send("delivery-delivery-asap",event.getDelivery().getId(), deliveryCreatedEventDto);
                break;
        }
    }
}
