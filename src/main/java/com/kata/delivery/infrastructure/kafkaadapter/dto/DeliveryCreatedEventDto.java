package com.kata.delivery.infrastructure.kafkaadapter.dto;

import com.kata.delivery.domain.event.DeliveryCreatedEvent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeliveryCreatedEventDto {
    private String deliveryId;
    private String deliveryName;
    private String deliveryDescription;
    private String deliveryMode;
    private String deliveryAddress;
    private String orderId;
    private String timeSlotId;
    private String userId;
    private LocalDateTime pushlishedAt;

    public DeliveryCreatedEventDto(DeliveryCreatedEvent deliveryCreatedEvent) {
        this.deliveryId = deliveryCreatedEvent.getDelivery().getId();
        this.deliveryName = deliveryCreatedEvent.getDelivery().getName();
        this.deliveryDescription = deliveryCreatedEvent.getDelivery().getDescription();
        this.deliveryMode = deliveryCreatedEvent.getDelivery().getDeliveryMode().toString();
        this.deliveryAddress = deliveryCreatedEvent.getDelivery().getAddress();
        this.orderId = deliveryCreatedEvent.getDelivery().getOrder().id();
        this.timeSlotId = deliveryCreatedEvent.getDelivery().getTimeSlotId();
        this.userId = deliveryCreatedEvent.getDelivery().getClientId();
        this.pushlishedAt = deliveryCreatedEvent.getPublishedAt();
    }
}
