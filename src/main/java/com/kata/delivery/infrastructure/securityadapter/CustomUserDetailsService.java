package com.kata.delivery.infrastructure.securityadapter;


import com.kata.delivery.infrastructure.mongoadapter.repository.ClientEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Collection;

@Slf4j
@Service
public class CustomUserDetailsService implements ReactiveUserDetailsService {

    private final ClientEntityRepository clientEntityRepository;

    public CustomUserDetailsService(ClientEntityRepository clientEntityRepository) {
        this.clientEntityRepository = clientEntityRepository;
    }

    @Override
    public Mono<UserDetails> findByUsername(String username) {
        return clientEntityRepository.findByUsername(username)
                .map(userEntity -> {
                    Collection<GrantedAuthority> authorities = new ArrayList<>();
                    authorities.add(new SimpleGrantedAuthority(userEntity.role().toString()));
                    return new org.springframework.security.core.userdetails.User(
                            userEntity.username(),
                            userEntity.password(),
                            authorities
                    );
                });
    }
}
