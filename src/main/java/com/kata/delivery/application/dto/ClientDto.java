package com.kata.delivery.application.dto;

public record ClientDto(String username, String email, String address, String phone) {
}
