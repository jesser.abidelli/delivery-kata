package com.kata.delivery.application.dto;

public record ClientSignUp(String username, String email, String adress, String phone,String password) {
}
