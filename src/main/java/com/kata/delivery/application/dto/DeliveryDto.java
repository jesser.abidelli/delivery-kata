package com.kata.delivery.application.dto;

import com.kata.delivery.domain.model.DeliveryMode;


public record DeliveryDto(String id, String name, String description, DeliveryMode deliveryMode, String address, OrderDto order, String timeSlotId, String clientId) {
}