package com.kata.delivery.application.dto;

import com.kata.delivery.domain.model.Item;
import java.util.List;

public record OrderDto(String id, String clientId, List<Item> items) {
}
