package com.kata.delivery.application.exceptionhandler;

import com.kata.delivery.domain.exceptions.DeliveryNotFoundException;
import com.kata.delivery.domain.exceptions.OrderNotFoundException;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.*;

import java.nio.file.AccessDeniedException;

@RestControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(BadCredentialsException.class)
    public String handleHttpClientErrorException(BadCredentialsException e) {
        log.error("error occurred",e);
        return ExceptionUtils.getRootCause(e).getMessage();
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(DeliveryNotFoundException.class)
    public String handleDeliveryNotFoundException(DeliveryNotFoundException e) {
        log.error("error occurred",e);
        return ExceptionUtils.getRootCause(e).getMessage();
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(OrderNotFoundException.class)
    public String handleOrderNotFoundException(OrderNotFoundException e) {
        log.error("error occurred",e);
        return ExceptionUtils.getRootCause(e).getMessage();
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(AccessDeniedException.class)
    public String handleAccessDeniedException(AccessDeniedException e) {
        log.error("error occurred from controller advice",e);
        return ExceptionUtils.getRootCause(e).getMessage();
    }
}
