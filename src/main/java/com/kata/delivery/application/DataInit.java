package com.kata.delivery.application;

import com.kata.delivery.domain.model.*;
import com.kata.delivery.domain.port.api.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
public class DataInit implements CommandLineRunner {
    private final ProductManagementService productManagementService;
    private final ItemManagementService itemManagementService;
    private final OrderManagementService orderManagementService;
    public final DeliveryManagementService deliveryManagementService;
    public final ClientService clientService;

    public DataInit(ProductManagementService productManagementService, ItemManagementService itemManagementService, OrderManagementService orderManagementService, DeliveryManagementService deliveryManagementService, ClientService clientService) {
        this.productManagementService = productManagementService;
        this.itemManagementService = itemManagementService;
        this.orderManagementService = orderManagementService;
        this.deliveryManagementService = deliveryManagementService;
        this.clientService = clientService;
    }

    @Override
    public void run(String... args) throws Exception {
        Client client1 = new Client("user", "user@gmail.com","password","address","phone", Role.USER);
        clientService.createClient(client1).block();

        Client client2 = new Client("admin", "admin@gmail.com","admin","address","phone", Role.ADMIN);
        clientService.createClient(client2).block();

        Product product3 = new Product("Product 3",3.9, "Product 3 description");
        Product product4 = new Product("Product 4", 4.0,"Product 4 description");

        Product product1Created = productManagementService.createProduct(product3).block();
        Product product2Created = productManagementService.createProduct(product4).block();

        Item item1 = itemManagementService.createItem(1, product1Created.id()).block();
        Item item2 = itemManagementService.createItem(2, product2Created.id()).block();

        List<Item> items = List.of(item1, item2);

        Mono<Order> order = orderManagementService.createOrder("user", items);

        Delivery delivery = new Delivery("Delivery 2", "Delivery 2 description", null, "Address 2", order.block(), null ,"user");

        deliveryManagementService.createDelivery(delivery, "user").subscribe();

        Delivery delivery1 = new Delivery("Delivery diff", "Delivery diff description", null, "Address diff", order.block(), null ,"user");

        deliveryManagementService.createDelivery(delivery1, "user").subscribe();
    }
}
