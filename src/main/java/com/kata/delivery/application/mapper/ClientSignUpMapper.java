package com.kata.delivery.application.mapper;

import com.kata.delivery.application.dto.ClientSignUp;
import com.kata.delivery.domain.model.Client;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ClientSignUpMapper {
    ClientSignUp toDto(Client client);
    Client toDomain(ClientSignUp clientSignUp);
}
