package com.kata.delivery.application.mapper;

import com.kata.delivery.application.dto.DeliveryDto;
import com.kata.delivery.domain.model.Delivery;
import org.mapstruct.Mapper;
@Mapper(componentModel = "spring")
public interface DeliveryDtoMapper {
    DeliveryDto toDto(Delivery delivery);
    Delivery toDomain(DeliveryDto deliveryDto);
}