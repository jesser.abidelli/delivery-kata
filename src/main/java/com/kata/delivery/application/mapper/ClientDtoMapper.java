package com.kata.delivery.application.mapper;

import com.kata.delivery.application.dto.ClientDto;
import com.kata.delivery.domain.model.Client;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ClientDtoMapper {
    ClientDto toDto(Client client);
    Client toDomain(ClientDto clientDto);
}