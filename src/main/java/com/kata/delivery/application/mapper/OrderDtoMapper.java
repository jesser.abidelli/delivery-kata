package com.kata.delivery.application.mapper;


import com.kata.delivery.application.dto.OrderDto;
import com.kata.delivery.domain.model.Order;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OrderDtoMapper {
    OrderDto toDto(OrderDto orderDto);
    Order toDomain(OrderDto orderDto);
}
