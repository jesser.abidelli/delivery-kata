package com.kata.delivery.application.rest;

import com.kata.delivery.application.dto.DeliveryDto;
import com.kata.delivery.application.mapper.DeliveryDtoMapper;
import com.kata.delivery.domain.port.api.DeliveryManagementService;
import jakarta.validation.Valid;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;


@CrossOrigin(origins = "*")
@RequestMapping("/api/delivery")
@RestController
@Validated
public class DeliveryController {
    private final DeliveryManagementService deliveryManagementService;

    private final DeliveryDtoMapper deliveryDtoMapper;

    public DeliveryController(DeliveryManagementService deliveryManagementService, DeliveryDtoMapper deliveryDtoMapper) {
        this.deliveryManagementService = deliveryManagementService;
        this.deliveryDtoMapper = deliveryDtoMapper;
    }

    @PostMapping
    public Mono<DeliveryDto> createDelivery(@RequestParam String clientId, @RequestParam String orderId, @Valid @RequestBody DeliveryDto deliveryDto) {
        return deliveryManagementService.createDelivery(deliveryDtoMapper.toDomain(deliveryDto), clientId,orderId)
                .map(deliveryDtoMapper::toDto);
    }

    @GetMapping("/{id}")
    public Mono<DeliveryDto> getDeliveryRecord(@PathVariable String id) {
        return deliveryManagementService.getDelivery(id)
                .map(deliveryDtoMapper::toDto);
    }

    @GetMapping("/all")
    public Flux<DeliveryDto> getAllRecord() {
        return deliveryManagementService.getDeliveries()
                .map(deliveryDtoMapper::toDto);
    }

    @PutMapping("/deliveryMode/{id}")
    public Mono<DeliveryDto> setDeliveryModeRecord(@PathVariable String id, @RequestBody String deliveryMode) {
        return deliveryManagementService.setDeliveryMode(id, deliveryMode)
                .map(deliveryDtoMapper::toDto);
    }

    @PutMapping("/deliveryTimeSlot/{id}")
    public Mono<DeliveryDto> setDeliveryTimeSlotRecord(@PathVariable String id, @RequestBody String start) {
        LocalDateTime startDateTime = LocalDateTime.parse(start);
        return deliveryManagementService.setDeliveryTimeSlot(id, startDateTime)
                .map(deliveryDtoMapper::toDto);
    }
}