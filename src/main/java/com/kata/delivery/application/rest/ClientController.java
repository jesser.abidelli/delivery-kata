package com.kata.delivery.application.rest;

import com.kata.delivery.application.dto.ClientDto;
import com.kata.delivery.application.dto.ClientSignUp;
import com.kata.delivery.application.mapper.ClientDtoMapper;
import com.kata.delivery.application.mapper.ClientSignUpMapper;
import com.kata.delivery.domain.port.api.ClientService;
import jakarta.validation.Valid;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@CrossOrigin(origins = "*")
@RequestMapping("/api/client")
@RestController
@Validated
public class ClientController {
    private final ClientService clientService;

    private final ClientDtoMapper clientDtoMapper;

    private final ClientSignUpMapper clientSignUpMapper;

    public ClientController(ClientService clientService, ClientDtoMapper clientDtoMapper, ClientSignUpMapper clientSignUpMapper) {
        this.clientService = clientService;
        this.clientDtoMapper = clientDtoMapper;
        this.clientSignUpMapper = clientSignUpMapper;
    }

    @PostMapping
    public Mono<ClientDto> createClient(@Valid @RequestBody ClientSignUp clientSignUp) {
        return clientService.createClient(clientSignUpMapper.toDomain(clientSignUp))
                .map(clientDtoMapper::toDto);
    }

    @GetMapping("/{id}")
    public Mono<ClientDto> getClientRecord(@PathVariable String id) {
        return clientService.getClient(id)
                .map(clientDtoMapper::toDto);
    }

    @PutMapping
    public Mono<ClientDto> updateClientRecord(@Valid @RequestBody ClientDto clientDto) {
        return clientService.updateClient(clientDtoMapper.toDomain(clientDto))
                .map(clientDtoMapper::toDto);
    }

    @DeleteMapping("/{id}")
    public void deleteClientRecord(@PathVariable String id) {
        clientService.deleteClient(id);
    }

    @GetMapping("/all")
    public Flux<ClientDto> getAllRecord() {
        return clientService.getClients()
                .map(clientDtoMapper::toDto);
    }
}
